# Changelog

## 2020-09-13

- Support émissions avec plusieurs diffusions par semaine

## 2020-08-06

- Les fonctionnalités de base sont opérationelles,
- La pige CSA est enregistrée en wav 16b stéréo 44,1kHz et non en MP3 mono 44kbps,
- Le programme n'a pas encore été testé environnement et conditions de production.
