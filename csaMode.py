import datetime
import time
from WeekDays import WeekDays
from EcasoundRecord import EcasoundRecord
from utils import eprint

def csaModeSubroutine(wdir) :
    currentHour = datetime.datetime.now().replace(minute=0, second=0, microsecond=0)
    nextHour    = currentHour + datetime.timedelta(hours=1)
    while True :
        nowDate = datetime.datetime.now()
        if ( nowDate > currentHour ) and ( nowDate < nextHour ) :
            recName = (f"{nowDate.year}-{nowDate.month:02d}-{nowDate.day:02d}-{WeekDays.intToFr(nowDate.weekday())}-{nowDate.hour:02d}h{nowDate.minute:02d}")
            erec = EcasoundRecord(currentHour, nextHour, recName, compressFormat="mp3", compressBitRate=48, compressChanN=1, wd=wdir)
            erec.threadedRun()
            currentHour = nextHour
            nextHour = currentHour + datetime.timedelta(hours=1)
        else:
            time.sleep(0.3)
