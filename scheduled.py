import datetime
import time
from PlannedRecord import PlannedRecord
from utils import eprint
from EcasoundRecord import EcasoundRecord

def scheduledSubroutine(wdir, jsonConf) :
    circularQueue = []
    # Build objets from json and append them to the clist.
    jsonList = jsonConf["enregistrements"]
    for i in jsonList :
        rec = PlannedRecord(i)
        if rec.isValidState :
            circularQueue.append(rec)
    if(len(circularQueue) == 0):
        eprint("Erreur : Il n'y a aucun enregistrement valide dans la file. Arrêt.")
        exit(7)

    # Check periodically if a record shall start
    print("Attente d'un enregistrement valide")
    while(True):
        for record in circularQueue :
            if(  datetime.datetime.now() > record.getEpochStart() 
             and datetime.datetime.now() < record.getEpochEnd() 
             and not record.isLaunched() ):
            # Start threaded recording operation
                erec = EcasoundRecord ( record.getEpochStart()
                                      , record.getEpochEnd()
                                      , record.getFilename()
                                      , pRec=record, wd=wdir 
                                      )
                erec.threadedRun()
        time.sleep(0.05)
