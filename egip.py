#!/bin/python3
# -*- coding: utf-8 -*-


# External Dependencies
import os, sys, os.path
import argparse
import json
# Internal Dependencies
from csaMode import csaModeSubroutine
from scheduled import scheduledSubroutine
from utils import eprint

# Main function
def main() :
    # Parsing arguments
    p = argparse.ArgumentParser(description="Utilitaire de pige programmable pour les radios françaises")
    p.add_argument ( "-c"
                   , "--csa"
                   , action="store_true"
                   , dest="csa_mode"
                   , help = "activer le mode CSA"
                   , default = False
                   )

    p.add_argument( "-d", "--directory"
                  , dest = "dir"
                  , help = "définit le répertoire d'écriture"
                  , default = os.getcwd()
                  )

    p.add_argument( 'confFile'
                  , nargs= '?'
                  ) 
    args = p.parse_args()

    # Guard : Check if directory exists and is writeable
    if not os.access(args.dir, os.W_OK) :
        eprint("Erreur : Le répertoire de sortie que vous avez spécifié n'est pas accessible en écriture.")
        sys.exit(4)
    elif args.dir.endswith('/') :
        args.dir = args.dir[:-1]
    
    # CSA Mode branching
    if args.csa_mode :
        # Start the csa_mode recording
        print("Starting with CSA_mode...")
        csaModeSubroutine(args.dir)
    
    # Guard : Check if the configuration JSON file is present when -c tag is missing
    elif args.confFile == None :
        eprint("Erreur : Le fichier de configuration est manquant.")
        sys.exit(1)
    elif not os.path.isfile(args.confFile) :
        eprint("Erreur : Le fichier que vous avez utilisé n'existe pas.")
        sys.exit(2)
    else:
        # Import JSON File
        try:    
            json_conf_file = open(args.confFile)
        except:
            eprint("Erreur : Le fichier spécifié n'est pas accessible en lecture")
            sys.exit(5)
        try:
            json_conf = json.load(json_conf_file)
            json_conf_file.close();
        except:
            eprint("Erreur : Le fichier que vous avez spécifié n'est pas au format JSON")
            sys.exit(3)
        try:
            json_conf['enregistrements']
        except:
            eprint("Erreur : Le fichier que vous avez spécifié ne respecte pas le format de données du programme")
            sys.exit(6)
        
        scheduledSubroutine(args.dir, json_conf)
        
     
# Main call    
main()
