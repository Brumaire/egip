from utils import eprint
from threading import Thread
import os
import signal
import subprocess
import time
import datetime

class EcasoundRecord :
    """The purpose of this class is to prepare (and eventually execute) a recording using the programm Ecasound."""
    
    def __init__( self,
                  startDate,
                  endDate,
                  outputName,
                  samplingBits=16,
                  samplingChan=2,
                  samplingFreq=44100,
                  recDevice="alsa",
                  pRec=None, wd=".",
                  compressFormat="",
                  compressChanN=1,
                  compressBitRate=320):

        self.wd              = wd
        self.pid             = -1
        self.startDate       = startDate 
        self.endDate         = endDate
        self.outputName      = outputName
        self.pRec            = pRec
        
        self.samplingBits    = samplingBits
        self.samplingChan    = samplingChan
        self.samplingFreq    = samplingFreq
        self.recDevice       = recDevice

        self.compressFormat  = compressFormat
        self.compressChanN   = compressChanN
        self.compressBitRate = compressBitRate

        
    def record(self) :
        """ The recording prodecure who deleguate to ecasound"""
        if(self.pRec is not None):
            self.pRec.launch()
        while datetime.datetime.now() < self.startDate :
            time.sleep(0.01)
        FNULL = open(os.devnull, 'w')
        proc = subprocess.Popen(["ecasound"
                                , "-i" , self.recDevice
                                , "-o" , self.fullRecOutputName()
                                ], stdout=FNULL)
        print(f"{datetime.datetime.now().strftime('%Y %b %d %H:%M:%S')} : Début de l'enregistrement {self.outputName}")
        print(f"Fin prévue : {self.endDate.strftime('%Y %b %d %H:%M:%S')} ")
        while datetime.datetime.now() < self.endDate :
            time.sleep(0.01)

        os.kill(proc.pid,signal.SIGINT)
        print(f"{datetime.datetime.now().strftime('%Y %b %d %H:%M:%S')} : Fin enregistrement {self.outputName}")
        time.sleep(2)
        if self.compressFormat != "" :
            print(f"Compression de {self.fullRecOutputName()} : {self.compressFormat}, {self.compressChanN} canal, {self.compressBitRate}kbps")
            proc2 = subprocess.Popen(["ffmpeg"
                                     , "-nostdin"
                                     , "-i"     , self.fullRecOutputName()
                                     , "-acodec", self.compressFormat
                                     , "-ab"    , str(self.compressBitRate)+'K'
                                     , "-ac"    , str(self.compressChanN)
                                     , self.fullCompressedOutputName() 
                                     ], stdout=FNULL, stderr=FNULL)
            proc2.wait()
            if(proc2.poll() == 0 ):
                print(f"Compression réussie, suppression du fichier original {self.fullRecOutputName}")
                os.remove(self.fullRecOutputName())

        if(self.pRec is not None):
            self.pRec.stop()

        FNULL.close()

    def threadedRun(self) :
        """ Lauch the recording method in a separate thread. """
        thread = Thread(target = self.record) 
        thread.start() 

    def fullRecOutputName(self) :
        return self.wd + '/' + self.outputName + ".wav"

    def fullCompressedOutputName(self) :
        result = ""
        if self.compressFormat != "" :
            result = self.wd + '/' + self.outputName + '.' + self.compressFormat
        return result
