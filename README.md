# Lisez-Moi

Egip (merci à Xavier pour le nom :) ) est un petit projet python à destination 
des radios associatives françaises dont le but est double :
- Faciliter la mise en place d'une pige conforme CSA.
- Automatiser l'enregistrement des émissions live pour la rediffusion.

Mais aussi de manière générale, réaliser toutes sortes d'enregistrement sur des plages de grille définies (ex : vendredi 8h-10h).

Attention : Cette version est une version de développement rolling. En l'absence de tests, il est déconseillé de la déployer en production. 
Si vous le faites, c'est à vos risques et périls et je ne peux pas garantir que son comportement soit conforme aux spécifications ci-dessous 
ou que votre ordinateur ne va pas exploser quand vous la lancerez.

Les retours sont les bienvenus et je peux l'étendre pour répondre à des besoins plus spécifiques, mais vous payez le café :p

Le programme fonctionne selon deux modes : 

- Le mode CSA, qui produit des enregistrements 24-7 conforme à la règlementation du Conseil Supérieur de L'Audiovisuel, à des fins légales.
Ce mode n'est pas programmable dans la mesure ou il répond à des contraintes strictes.

- Le mode Emission, programmable, qui produit des enregistrements Wav 44,1 kHz.
(la possibilité de modifier l'échantillonage et la compression est prévue mais non implémentée).

## Equipe

- Lucas Vincent <Lucas.vincent@entalpi.net>

## Plateforme

- Systèmes UNIX (testée sur GNU/Linux exclusivement)

## Dépendances 

- python > 3.8
- ecasound (backend pour les enregistrements, installez le via le gestionnaire de paquet de votre distro).
- ffmpeg (backend pour la compression)

## Installation

- Git clone ou wget la branche dans le répertoire de votre choix.

## Environnement d'exécution

Si vous voulez utiliser le mode programmable et enregistrer en wav, il est conseillé de lancer cette application en mode privilégié (root) 
pour que Ecasound puisse profiter des fonctionnalités RT / temps réel du noyau linux. Dans le cas contraire vous pouvez perdre des échantillons.
Attention à ce que ça peut impliquer pour la sécurité de votre machine.

Ce n'est pas nécéssaire pour le mode CSA.

## Entrées 

`egip.py [-cd] [confFile.json]`

### Arguments :

Le programme prend les options suivantes :

- --directory -d : Répertoire de destination, par défaut, il s'agit du répertoire courant depuis lequel est lancé le programme.

- --csa -c : Lance le programme en mode CSA.

Si l'option --csa n'est pas fournie, alors il faut donner au programme en dernier paramètre le chemin du fichier JSON de configuration.

### Données

L'entrée principale du programme est un fichier JSON comportant une liste d'objets *enregistrement*. 

Exemple d'un objet *enregistrement* :

``` JSON
{
    "tag": "descriptif"
	"joursSemaine": ["mardi"],
	"heureDebut": "08:00",
	"heureFin": "09:00"
}
```

Signification des champs :

- tag : Annotation présente dans le nom du fichier,
- joursSemaine : Les jours de la semaine concernés,
- heureDebut : L'heure de début de l'enregistrement,
- heureFin : L'heure de fin de l'enregistrement.

Le temps est traité en **heure locale**.

Ces champs ne bougeront plus, mais d'autres (optionnels) viendront les compléter pour surcharger certains paramètres
ou permettre des enregistrement à une fréquence autre qu'hebdomadaire.

Exemple de fichier d'entrée :

```JSON
{
	"enregistrements": [
		{
			"tag": "Dawa Station",
			"joursSemaine": ["lundi"],
			"heureDebut": "09:00",
			"heureFin": "10:00"
			
		},
		{
			"tag": "Punch en Musique",
			"jourSemaine": "lundi",
			"heureDebut": "11:00",
			"heureFin": "12:00"
			
		}
	]
}
```

## Sorties 

Le programme produit des enregistrements sonores, sous forme de fichiers dans le répertoire spécifié.

Généralement, le nom du fichier est au format suivant :
- YYYY-MM-DD-weekday-tag.ext

### Pige CSA

Exemple :
```
2020-08-04-lundi-10h00.mp3
```

### Pige Emissions

Exemple :
```
2020-08-04-lundi-22h30-Dawastation.wav
```

## Evolutions futures / Todolist

- Petite appli compagnonne web pour générer les JSON avec une IHM sympa.
- Image docker
- Service systemd
- 


