import datetime
import sys

from utils import eprint
from WeekDays import WeekDays

class PlannedRecord :
    """ A recording project. Beware : The consistency of Start Time and End Time value is only guarenteed BEFORE the start time."""
    def __init__ (self, recordDict) :
        self.isValidState = True
        self.isLaunchedState = False
        tag = "undefined"
        try:
            self.tag  = recordDict['tag']
            self.weekDays   = recordDict['joursSemaine']
            self.weekDaysInts = []
            self.startTime = recordDict['heureDebut']
            self.endTime   = recordDict['heureFin']

            self.startHour   = self.getHour(self.startTime)
            self.startMinute = self.getMinute(self.startTime)
            self.endHour     = self.getHour(self.endTime)
            self.endMinute   = self.getMinute(self.endTime)
            
            # Guard : 0 < hour < 24
            if not self.startHour in range(0,24) or not self.endHour in range(0,24):
                eprint(f"Warning : L'enregistrement {self.tag} comporte une heure invalide et ne peut donc être traité.")
                self.isValidState = False
            # Guard 0 < min < 60
            if not self.startMinute in range(0,60) or not self.endMinute in range(0,60) :
                eprint(f"Warning : L'enregistrement {self.tag} comporte une minute invalide et ne peut donc être traité.")
                self.isValidState = False

            # Guard Week Day consistency
            for i in self.weekDays:
                if not WeekDays.hasValue(i) :
                    eprint(f"Warning : L'enregistrement {self.tag} comporte un jour de la semaine incorrect et ne peut donc être traité.")
                    self.isValidState = False
                else :
                    self.weekDaysInts.append(WeekDays.frToInt(i))
        except:
            eprint(f"Warning : L'enregistrement {self.tag} n'est pas formaté correctement et ne peut donc être traité.")
            self.isValidState = False
            
    def getHour (self, timeString) :
        return int(timeString.split(':')[0])

    def getMinute (self, timeString) :
        return int(timeString.split(':')[1])

    def isToday (self) :
        """ Is the recording scheduled for today """
        now = datetime.datetime.now()
        return now.weekday() in self.weekDayInts

    def startDay (self) :
        """ Compute the real start day of the recording from scheduled week day """
        now = datetime.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
        relDays = list ( map (lambda day : day - now.weekday(), self.weekDaysInts) )
        for i in range(0,len(relDays) ) :
           if relDays[i] < 0 :
                relDays[i]=relDays[i]+7
        return now + datetime.timedelta(days=min(relDays)) 

    def endDay (self) :
        res = self.startDay()
        """ Compute the real end day of the recording """
        if (self.endHour * 60 + self.endMinute) < (self.startHour * 60 + self.startMinute) :
            res = res + datetime.timedelta(days=1)
        return res
        
    def getFilename (self) :
        """ Get the record desired filename """
        now = datetime.datetime.now()
        return f"{now.year}-{now.month:02d}-{now.day:02d}-{WeekDays.intToFr(now.weekday())}-{self.formatedStartTime()}-{self.tag}" 
        
    def getEpochStart (self):
        """ Get the time when the recording shall end """
        return ( self.startDay().replace(hour=self.startHour, minute=self.startMinute ) )
        
    def getEpochEnd (self) :
        """ Get the time the recording shall end """
        return ( self.endDay().replace(hour=self.endHour, minute=self.endMinute ) )

    def getDuration (self) :
        """ Get the duration of the recording"""
        return self.getEpochEnd() - self.getEpochStart()

    def launch (self) :
        if self.isLaunchedState :
            eprint(f"Warning : L'enregistrement {self.tag} était déja lancé. Le programme est dans un état inconsistant.")
        self.isLaunchedState = True

    def stop (self) :
        if not self.isLaunchedState:
            eprint(f"Warning : L'enregistrement {self.tag} était déja lancé. Le programme est dans un état inconsistant.")
        self.isLaunchedState = False

    def isLaunched (self) :
        return self.isLaunchedState

    def formatedStartTime(self):
        return self.startTime.replace(':','h') 
