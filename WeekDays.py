class WeekDays:
    toInt     =   {   "lundi"    : 0
                  ,   "mardi"    : 1
                  ,   "mercredi" : 2
                  ,   "jeudi"    : 3
                  ,   "vendredi" : 4
                  ,   "samedi"   : 5
                  ,   "dimanche" : 6
                  }

    fromInt   =   {   0 : "lundi"
                  ,   1 : "mardi"
                  ,   2 : "mercredi"
                  ,   3 : "jeudi"
                  ,   4 : "vendredi"
                  ,   5 : "samedi"
                  ,   6 : "dimanche"
                  }

    @classmethod
    def hasValue(cls, value):
        return value.lower() in cls.toInt

    @classmethod 
    def frToInt(cls,value):
        return cls.toInt[value.lower()]

    @classmethod
    def intToFr(cls,value):
        return cls.fromInt[value]
